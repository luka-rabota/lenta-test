<?php

include_once(__DIR__ . '/../src/ArraySort.php');
include_once(__DIR__ . '/../src/EmptyArrayException.php');

use App\ArraySort;
use App\EmptyArrayException;
use PHPUnit\Framework\TestCase;

class ArraySortTest extends TestCase
{
    public function testSortCoupleWithExcepton()
    {
        $as = new ArraySort();

        $this->expectException(EmptyArrayException::class);

        $result = $as->sortCouple([3, 50, 131, 1305], []);
    }

    public function testSortCouple()
    {
        $as = new App\ArraySort();

        $result = $as->sortCouple([3, 50, 131, 1305], [-5, 10, 25, 50, 1111]);

        $this->assertEquals([-5, 3, 10, 25, 50, 50, 131, 1111, 1305], $result);
    }
}