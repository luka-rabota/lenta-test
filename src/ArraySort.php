<?php

namespace App;

class ArraySort
{
    private const FIRST_MASTER = 'first';
    private const SECOND_MASTER = 'second';

    /** @var int $buffer */
    private $buffer = 0;

    /** @var string $bufferMaster */
    private $bufferMaster = '';

    /** @var array $result */
    private $result = [];

    public function sortCouple(array $first, array $second): array
    {
        if (empty($first) || empty($second)) {
            throw new EmptyArrayException('Empty array'); 
        }

        $this->compareFirstElements(array_shift($first), array_shift($second));

        do {
            $current = $this->bufferMaster == self::FIRST_MASTER ? array_shift($second) : array_shift($first);

            if (!$current) {
                $current = self::FIRST_MASTER ? array_shift($first) : array_shift($second);
            }

            $this->compareCurrentWithBuffer($current);
        } while (!empty($first) || !empty($second));

        $this->result[] = $this->buffer;
        
        return $this->result;  
    }

    private function compareFirstElements(int $first, int $second): void
    {
        if ($first < $second) {
            $this->result[] = $first;

            $this->buffer = $second;
            $this->bufferMaster = self::SECOND_MASTER;
        
            return;
        }   
        
        $this->result[] = $second;

        $this->buffer = $first;
        $this->bufferMaster = self::FIRST_MASTER;
    }

    private function compareCurrentWithBuffer(int $current): void
    {
        if ($current < $this->buffer) {
            $this->result[] = $current;

            return;
        }

        $this->result[] = $this->buffer;

        $this->buffer = $current;
        $this->bufferMaster = $this->switchBufferMaster();
    }

    private function switchBufferMaster(): string
    {
        return $this->bufferMaster == self::FIRST_MASTER ? self::SECOND_MASTER : self::FIRST_MASTER;
    }
}